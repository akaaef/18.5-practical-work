﻿#include <iostream>
#include <string>
using namespace std;

class NameAndScore
{
private:
	string Name;
	int Score;
public:
	void Input()
	{
		cout << "Name: ";
		cin >> Name;
		cout << "Score: ";
		cin >> Score;
	}

	void Show()
	{
		cout << Name << ":" << Score << endl;
	}

	void sortPlayers(NameAndScore* players, int Size)
	{
		for (int i = 0; i < Size; i++) {
			
			int max_score = players[i].Score;
			int max_index = i;
			for (int j = i + 1; j < Size; j++) {
				if (players[j].Score > max_score) {
					max_score = players[j].Score;
					max_index = j;
				}
			}
			if (max_index != i) {
				std::swap(players[i], players[max_index]);
			}
		}
	}
};

int main()
{
	NameAndScore temp;
	int Size;
	cout << "Count Player: ";
	cin >> Size;
	NameAndScore* Array = new NameAndScore[Size];
	
	 for (int i = 0; i < Size; i++)
	 {
		Array[i].Input(); //Ввод данных
		
	 }

	 temp.sortPlayers(Array, Size);

	 for (int i = 0; i < Size; i++)
	 {
		 Array[i].Show();
	 }
	 delete[] Array;

}